# store is a singleton. use the_store
import json
import os
from datetime import datetime
from kivy.event import EventDispatcher
from kivy.clock import Clock

class Store(EventDispatcher):
    dirty = False
    persist_interval = 60 * 5
    text_inputs = []
    users = []
    products = []
    customers = []
    status = {
        'logged_in_user': {'id': -1, 'name': ''},
        'selected_customer': -1,
        'selected_item': -1,
        'last_persistance': '',
        'indizes': {
            'nextUsersIndex': -1,
            'nextProductsIndex': -1,
            'nextItemsIndex': -1,
            'nextCustomersIndex': -1,
            'nextCategoriesIndex': -1,
            'nextPaymentsIndex': -1
        }
    }

    def __init__(self, **kwargs):
        self.register_event_type('on_update')
        self.register_event_type('on_update_selected_customer')
        self.register_event_type('on_update_logged_in_user')
        self.register_event_type('on_update_customers')
        self.register_event_type('on_update_products')
        self.register_event_type('on_update_users')
        self.register_event_type('on_update_persist')
        self.register_event_type('on_persist_start')
        self.register_event_type('on_persist_end')
        super(Store, self).__init__(**kwargs)
        datetimestr = str(datetime.now())
        self.status['last_persistance'] = datetimestr[:datetimestr.index('.')]
        '''
        file modes: 
            'r': default, read
            'w': write. if exists, truncate, else create
            'x': create. if exists, error
            'a': append. if not exists, create
            't': default, open in text mode
            'b': open in binary
            '+': open for read and write (update)
        '''
        for x in ['users', 'products', 'customers', 'categories']:
            f = open('data/%s.json' % x, 'r', encoding = 'utf-8')
            if f.mode == 'r':
                data_string = f.read()
                data = json.loads(data_string)
                setattr(self, x, data[x])
                next_index_str = 'next%sIndex' % x.capitalize()
                self.status['indizes'][next_index_str] = data[next_index_str]
                if x == 'customers':
                    self.status['indizes']['nextItemsIndex'] = data['nextItemsIndex']
                    self.status['indizes']['nextPaymentsIndex'] = data['nextPaymentsIndex']
        Clock.schedule_once(self.persist_store, self.persist_interval)
    
    def persist_store(self, dt):
        if self.dirty:
            print('Persisting store...')
            self.dispatch('on_persist_start')
            for x in ['users', 'products', 'customers', 'categories']:
                result_dict = {}
                result_dict[x] = getattr(self, x)
                next_index_key = 'next%sIndex' % x.capitalize()
                result_dict[next_index_key] = self.status['indizes'][next_index_key]
                if x == 'customers':
                    result_dict['nextItemsIndex'] = self.status['indizes']['nextItemsIndex']
                    result_dict['nextPaymentsIndex'] = self.status['indizes']['nextPaymentsIndex']
                with open('data/%s.json_' % x, 'w') as outfile:
                    json.dump(result_dict, outfile)
                os.remove('data/%s.json' % x)
                os.rename('data/%s.json_' % x, 'data/%s.json' % x)
            self.dispatch('on_persist_end')
            datetimestr = str(datetime.now())
            self.status['last_persistance'] = datetimestr[:datetimestr.index('.')]
            self.dispatch('on_update_persist')
            print('Done persisting!')
        else:
            print('Not persisting store as it is not dirty')
        self.dirty = False
        Clock.schedule_once(self.persist_store, self.persist_interval)

    def on_update(self):
        pass
    
    def on_update_selected_customer(self):
        pass

    def on_update_logged_in_user(self):
        pass

    def on_update_customers(self):
        pass

    def on_update_products(self):
        pass

    def on_update_users(self):
        pass

    def on_update_persist(self):
        pass

    def on_persist_start(self):
        pass
    
    def on_persist_end(self):
        pass

    def dispatch_update(self, evt):
        self.dirty = True
        self.dispatch(evt)

    def get_by_key(self, dic, key, value):
        target_list = getattr(self, dic)
        result = list(filter(lambda entry : entry[key] == value, target_list))
        if len(result) == 1:
            return result[0]
        return None
    
    def get_by_id(self, dic, id):
        return self.get_by_key(dic, 'id', id)
    
    def get_by_name(self, dic, name):
        return self.get_by_key(dic, 'name', name)

    def add_textinput(self, ti):
        self.text_inputs.append(ti)

    def unfocus_textinputs(self, text_input = None):
        if not text_input or text_input.focus:
            for ti in self.text_inputs:
                if text_input != ti:
                    ti.focus = False

    def logout(self):
        self.status['logged_in_user'] = {
            'name': '',
            'id': -1
        }
        self.dispatch_update('on_update_logged_in_user')

the_store = Store()
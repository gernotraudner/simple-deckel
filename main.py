# required only for raspi
import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.textinput import TextInput
from kivy.config import Config
from kivy.core.window import Window
from kivy.lang import Builder
from screens.loginscreen import LoginScreen
from screens.overviewscreen import OverviewScreen
from screens.productsscreen import ProductsScreen
from screens.customersscreen import CustomersScreen
from screens.usersscreen import UsersScreen
from containers.logincontainer import LoginContainer
from containers.panel import Panel
from widgets.coloredlabel import ColoredLabel
from widgets.additempanel import AddItemPanel
from widgets.selectablelabel import SelectableLabel
from widgets.selectableitem import SelectableItem
from widgets.categoryentry import CategoryEntry
from widgets.productentry import ProductEntry
from widgets.customersdetailsentry import CustomersDetailsEntry
from widgets.productselectionentry import ProductSelectionEntry
from widgets.historyentry import HistoryEntry
from widgets.usersdetailsentry import UsersDetailsEntry
from widgets.customersrecycleview import CustomersRecycleView
from widgets.itemsrecycleview import ItemsRecycleView
from widgets.productsrecycleview import ProductsRecycleView
from widgets.categoriesrecycleview import CategoriesRecycleView
from widgets.customersdetailsrecycleview import CustomersDetailsRecycleView
from widgets.productselectionrecycleview import ProductSelectionRecycleView
from widgets.historyrecycleview import HistoryRecycleView
from widgets.usersdetailsrecycleview import UsersDetailsRecycleView
from widgets.productchangepopup import ProductChangePopup
from widgets.itemdetailspopup import ItemDetailsPopup
from widgets.addcustomerpopup import AddCustomerPopup
from widgets.addpaymentpopup import AddPaymentPopup
from widgets.passwordpopup import PasswordPopup
from widgets.historypopup import HistoryPopup
from widgets.additemspopup import AddItemsPopup
from widgets.adduserpopup import AddUserPopup
from widgets.reallypopup import ReallyPopup
from store.store import the_store
from functools import partial

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class SimpleDeckelApp(App):
    def build(self):
        Config.set('kivy', 'keyboard_mode', 'systemandmulti')
        Config.set('kivy', 'keyboard_layout', 'qwertz')
        Config.write()
        return ScreenManager()

    def _unfocus_textinputs(self, not_this_one):
        for w in self.root.walk():
            if isinstance(w, TextInput) and not (w == not_this_one):
                w.focus = False
                w.hide_keyboard()

if __name__ == '__main__':
    SimpleDeckelApp().run()


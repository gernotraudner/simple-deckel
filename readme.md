# Simple-Deckel

## Development in VSCode
* Install [Python 3.5.+](https://www.python.org/downloads/). Make sure to tick the __Add Python to PATH__ checkbox.
* Install the [Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python) by Microsoft.
* VSCode will ask for a linter. Unfortunately __pylint__ doesn't quite work with kivy, so ignore this.
* Follow instructions provided by [kivy](https://kivy.org/doc/stable/installation/installation-windows.html) (I don't think we need `gstreamer` though)
* Make sure to rename the `data/*.json.template` files into `data/*.json` to make the store work.
* The default login for the provided template files is `hans`, the password `wurst`.

## Building
### Windows
Just follow [these instructions](https://kivy.org/doc/stable/guide/packaging-windows.html). There's already a `.spec` file within the `bundle` directory, so after installing `PyInstaller` and updating the absolute path in `bundle/simple-deckel-windows.spec`, you should be able to just run `python -m PyInstaller simple-deckel-windows.spec`.
### Raspbian
This is a bit more complicated.
* Checkout this repo on your raspbian device
* Follow instructions provided by [kivy](https://kivy.org/doc/stable/installation/installation-rpi.html). __IMPORTANT:__ Make sure to run it with python3 and pip3!
* `pip3 install PyInstaller`
* go to `bundle` directory, then `python -m PyInstaller simple-deckel-raspbian.spec`

## Running
When running the executables, kivy will create a config file located at `<usr-home>/.kivy/config.ini`. There might be an issue with an invisible mouse cursor. This can be circumvented by running the app once, then killing it (you might not even be able to exit it using the escape key, since sometimes keyboard input is also not working) and modifying the `config.ini` according to [this stackoverflow post](https://stackoverflow.com/questions/43588499/kivy-mouse-cursor-not-showing#47581460).

## Remarks
Raspbian takes a bit of work to get kivy running. 
* You'll most likely run into a `Segmentation fault` error because of the graphics adapter on raspbian. This is described and workarounded on this [Github page](https://github.com/kivy/kivy/issues/6007)
* This app is designed for a touchscreen. Keyboard input might not work as expected. I had lots of issues with defocussing `TextInputs` and styling the `VKeyboard`, so this is the result.

## Translation
If you need a bit of help in finding out what means what ;) Here's a bit of vocabulary:
Kunden = customers
Benutzer = user(s)
Produkte = products
verwalten = manage
Vergangene Rechnungen = (basically) billing history
Bezahlen = pay
Abbrechen = cancel
Bestätigen = confirm


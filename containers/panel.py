from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class Panel(BoxLayout):
    def __init__(self, **kwargs):
        super(Panel, self).__init__(**kwargs)
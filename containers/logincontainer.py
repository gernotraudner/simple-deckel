from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.event import EventDispatcher
from store.store import the_store
from tools.hashing import verify_password
Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class LoginContainer(BoxLayout, EventDispatcher):
    username = ObjectProperty(None)
    password = ObjectProperty(None)
    errorhint = ObjectProperty(None)
    def __init__(self, **kwargs):
        self.register_event_type('on_login')
        super(LoginContainer, self).__init__(**kwargs)
        Clock.schedule_once(self.on_widget_loaded)

    def on_widget_loaded(self, dt):
        self.username = self.ids.username
        self.username.bind(on_text_validate = self.on_submit)
        self.password = self.ids.password
        self.password.bind(on_text_validate = self.on_submit)
        self.loginbutton = self.ids.loginbutton
        self.loginbutton.bind(on_press = self.on_submit)
        self.errorhint = self.ids.errorhint

    def on_login(self):
        print('default login handler')

    def on_submit(self, *args):
        self.errorhint.text = ''
        errorhint = []
        if (self.username.text == ''):
            errorhint += ['Bitte Usernamen ausfüllen!']
        if (self.password.text == ''):
            errorhint += ['Bitte Passwort ausfüllen!']
        filtered_users = list(filter(lambda user: user['name'] == self.username.text and verify_password(user['password'], self.password.text), the_store.users))
        loginSuccessful = len(filtered_users) == 1
            
        if (len(errorhint) != 0):
            self.errorhint.text = '\n'.join(errorhint)
        elif not loginSuccessful:
            self.errorhint.text = 'Username oder Passwort falsch!'
        else:
            the_store.status['logged_in_user']['id'] = filtered_users[0]['id']
            the_store.status['logged_in_user']['name'] = filtered_users[0]['name']
            the_store.dispatch_update()
            self.username.text = ''
            self.password.text = ''
            self.dispatch('on_login')
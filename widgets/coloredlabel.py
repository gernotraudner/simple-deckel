from kivy.lang import Builder
from kivy.properties import ColorProperty, NumericProperty
from kivy.uix.label import Label

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ColoredLabel(Label):
    bg_color = ColorProperty([0, 0, 0, 0])
    _scale = NumericProperty(1)

    def __init__(self, **kwargs):
        super(ColoredLabel, self).__init__(**kwargs)

    #def on_text(self, *_):


from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from kivy.factory import Factory
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class HistoryPopup(Popup):
    really_popup = ObjectProperty(None)
    def __init__(self, **kwargs):
        super(HistoryPopup, self).__init__(**kwargs)
        self.really_popup = Factory.ReallyPopup()
        self.really_popup.bind(on_accepted = self.clear_customer)

    def open_really_popup(self):
        customer = the_store.get_by_id('customers', the_store.status['selected_customer'])
        self.really_popup.question_text = 'Daten von "%s" wirklich löschen?' % customer['name']
        self.really_popup.open()

    def clear_customer(self, node):
        customer = the_store.get_by_id('customers', the_store.status['selected_customer'])
        customer['items'].clear()
        customer['payments'].clear()
        the_store.dispatch_update('on_update_customers')

from kivy.uix.recycleview import RecycleView
from kivy.lang import Builder
from store.store import the_store

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class ProductsRecycleView(RecycleView):
    def __init__(self, **kwargs):
        super(ProductsRecycleView, self).__init__(**kwargs)
        the_store.bind(on_update_products = self.load_products)
        self.load_products(self)

    def product_to_data_entry(self, product):
        creator = 'Unbekannt'
        try:
            creator = the_store.get_by_id('users', product['createdBy'])['name']
        except:
            pass
        return {
            'p_name': product['name'],
            'p_price': '%.2f' % product['price'],
            'p_category': product['category'],
            'p_created_by': creator,
            'p_timestamp': product['timestamp'],
            'p_color': product['color'],
            'product_id': product['id']
        }
    
    def load_products(self, caller):
        result = list(map(self.product_to_data_entry, the_store.products))
        sorted_list = sorted(result, key = lambda product : product['p_name'])
        sorted_list.insert(0, {
            'p_name': 'Produkt',
            'p_price': 'Preis',
            'p_category': 'Kategorie',
            'p_created_by': 'Ersteller',
            'p_timestamp': 'Hinzugefügt am',
            'p_color': [1, 1, 1, 1],
            'product_id': -1
        })
        self.data = sorted_list

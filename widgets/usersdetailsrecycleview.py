from kivy.uix.recycleview import RecycleView
from kivy.lang import Builder
from store.store import the_store
from functools import reduce
Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class UsersDetailsRecycleView(RecycleView):
    def __init__(self, **kwargs):
        super(UsersDetailsRecycleView, self).__init__(**kwargs)
        the_store.bind(on_update_users = self.load_users)
        self.load_users(self, True)

    def user_to_data_entry(self, user):
        try:
            creator = the_store.get_by_id('users', user['createdBy'])['name']
        except:
            creator = 'Unbekannt'

        return {
            'u_name': user['name'],
            'u_created_by': creator,
            'u_timestamp': user['timestamp'],
            'u_id': user['id']
        }
    
    def load_users(self, caller, initial = False):
        result = list(map(self.user_to_data_entry, the_store.users))
        sorted_list = sorted(result, key = lambda user : user['u_name'])
        sorted_list.insert(0, {
            'u_name': 'Name',
            'u_created_by': 'Erstellt von',
            'u_timestamp': 'Erstellt am',
            'u_id': -1
        })
        self.data = sorted_list

from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class CustomersDetailsEntry(RecycleDataViewBehavior, BoxLayout):
    index = None
    
    c_name = StringProperty('')
    c_total = StringProperty('')
    c_last_order_date = StringProperty('')
    c_last_payment = StringProperty('')
    c_created_by = StringProperty('')
    c_timestamp = StringProperty('')
    c_id = NumericProperty(-1)
    really_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(CustomersDetailsEntry, self).__init__(**kwargs)
        self.really_popup = Factory.ReallyPopup()
        self.really_popup.bind(on_accepted = self.delete_customer)
    
    def open_really_popup(self):
        self.really_popup.question_text = 'Kunde "%s" wirklich löschen?' % self.c_name
        self.really_popup.open()

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(CustomersDetailsEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(CustomersDetailsEntry, self).refresh_view_layout(rv, index, layout, viewport)

    def delete_customer(self, node):
        customer = the_store.get_by_id('customers', self.c_id)
        the_store.customers.remove(customer)
        if self.c_id == the_store.status['selected_customer']:
            the_store.status['selected_customer'] = -1
            the_store.dispatch_update('on_update_selected_customer')
        the_store.dispatch_update('on_update_customers')

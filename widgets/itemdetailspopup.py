from kivy.uix.popup import Popup
from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ItemDetailsPopup(Popup, EventDispatcher):
    item_name = StringProperty('')
    item_amount = NumericProperty(-1)
    item_category = StringProperty('')
    item_total_price = NumericProperty(-1)
    item_created_by = StringProperty('')
    item_timestamp = StringProperty('')
    item_id = NumericProperty(-1)
    password_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ItemDetailsPopup, self).__init__(**kwargs)
        self.password_popup = Factory.PasswordPopup()
        self.password_popup.bind(on_approved = self.delete_item)

    def delete_item(self, node, username):
        customer = the_store.get_by_id('customers', the_store.status['selected_customer'])
        matching_items = list(filter(lambda item: item['id'] == self.item_id, customer['items']))
        if len(matching_items) == 1:
            customer['items'].remove(matching_items[0])
            self.dismiss()
            datetimestr = str(datetime.now())
            customer['lastTransaction'] = datetimestr[:datetimestr.index('.')]
            the_store.dispatch_update('on_update_customers')
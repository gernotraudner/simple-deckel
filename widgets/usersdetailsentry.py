from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class UsersDetailsEntry(RecycleDataViewBehavior, BoxLayout):
    index = None
    login_id = NumericProperty(-1)
    u_name = StringProperty('')
    u_created_by = StringProperty('')
    u_timestamp = StringProperty('')
    u_id = NumericProperty(-1)
    really_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(UsersDetailsEntry, self).__init__(**kwargs)
        the_store.bind(on_update_logged_in_user = self.assign_login_id)
        self.really_popup = Factory.ReallyPopup()
        self.really_popup.bind(on_accepted = self.delete_customer)
    
    def assign_login_id(self, caller):
        self.login_id = the_store.status['logged_in_user']['id']

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(UsersDetailsEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(UsersDetailsEntry, self).refresh_view_layout(rv, index, layout, viewport)

    def delete_customer(self, node):
        user = the_store.get_by_id('users', self.u_id)
        the_store.users.remove(user)
        the_store.dispatch_update('on_update_users')

    def open_really_popup(self):
        self.really_popup.question_text = 'Benutzer "%s" wirklich löschen?' % self.u_name
        self.really_popup.open()
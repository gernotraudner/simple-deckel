from kivy.uix.popup import Popup
from kivy.properties import NumericProperty, ObjectProperty
from kivy.event import EventDispatcher
from kivy.factory import Factory
from kivy.lang import Builder

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class AddPaymentPopup(Popup, EventDispatcher):
    password_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(AddPaymentPopup, self).__init__(**kwargs)
        self.password_popup = Factory.PasswordPopup()
        self.password_popup.bind(on_approved = lambda node, username: self.dispatch('on_save', amount = float(self.ids.ti_amount.text), username = username))
        self.register_event_type('on_save')

    def on_save(self, **args):
        pass
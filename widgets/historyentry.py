from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty, ColorProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class HistoryEntry(RecycleDataViewBehavior, BoxLayout):
    index = None
    h_name = StringProperty('')
    h_total = StringProperty('')
    h_timestamp = StringProperty('')
    h_created_by = StringProperty('')
    h_color = ColorProperty([1, 1, 1, 1])

    def __init__(self, **kwargs):
        super(HistoryEntry, self).__init__(**kwargs)
    
    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(HistoryEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(HistoryEntry, self).refresh_view_layout(rv, index, layout, viewport)
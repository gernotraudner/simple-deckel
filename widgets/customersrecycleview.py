from kivy.uix.recycleview import RecycleView
from kivy.properties import ObjectProperty, NumericProperty
from kivy.lang import Builder
from kivy.clock import Clock
from store.store import the_store
from datetime import datetime
Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class CustomersRecycleView(RecycleView):
    rv_layout = ObjectProperty(None)
    def __init__(self, **kwargs):
        super(CustomersRecycleView, self).__init__(**kwargs)
        the_store.bind(on_update_customers = self.load_customers)
        self.load_customers(self)
        Clock.schedule_once(self.on_widget_loaded)

    def load_customers(self, caller):
        customers = list(map(lambda customer: {'text': customer['name'], 'c_id': customer['id']}, the_store.customers))
        sorted_customers = sorted(customers, key = lambda customer: customer['text'])
        self.data = sorted_customers

    def datetime_from_string(self, string):
        return datetime.strptime(string, '%Y-%m-%d %H:%M:%S')

    def on_widget_loaded(self, dt):
        self.ids.selectable.bind(selected_value = self.on_select)

    def on_select(self, node, value):
        if not the_store.status['selected_customer'] is value:
            the_store.status['selected_customer'] = value
            the_store.dispatch_update('on_update_selected_customer')

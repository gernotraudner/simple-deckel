from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from store.store import the_store
from functools import partial
from tools.hashing import verify_password
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class PasswordPopup(Popup, EventDispatcher):
    hint_widget = ObjectProperty(None)
    init_height = NumericProperty(-1)
    hint_parent = ObjectProperty(None)
    
    def __init__(self, **kwargs):
        super(PasswordPopup, self).__init__(**kwargs)
        self.register_event_type('on_approved')

        Clock.schedule_once(self.on_widget_loaded)
        
    def on_widget_loaded(self, dt):
        self.ids.ti_password.bind(focus = self.reset_hint)
        self.ids.ti_username.bind(focus = self.reset_hint)
        self.init_height = self.height
        self.hint_parent = self.ids.lbl_hint.parent
        self.ids.lbl_hint.parent.remove_widget(self.ids.lbl_hint)
        self.height = 270

    def on_dismiss(self):
        self.reset_hint(None, None)
        self.ids.ti_password.text = ''

    def add_hint(self, text):
        self.hint_widget = Label(text=text)
        self.hint_parent.add_widget(self.hint_widget)
        self.height = self.init_height

    def reset_hint(self, instance = None, value = None):
        if self.hint_widget:
            self.hint_widget.text = ''
            if self.hint_widget.parent:
                self.hint_widget.parent.remove_widget(self.hint_widget)
        self.height = 270

    def on_approved(self, username):
        pass

    def validate_password(self):
        pw = self.ids.ti_password.text
        user_name = self.ids.ti_username.text
        user = the_store.get_by_name('users', user_name)
        if user is None:
            self.add_hint('Benutzername falsch!')
            return
        if (verify_password(user['password'], pw)):
            self.dispatch('on_approved', user_name)
            self.dismiss()
        else:
            self.add_hint('Passwort falsch!')
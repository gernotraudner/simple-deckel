from kivy.uix.popup import Popup
from kivy.event import EventDispatcher
from kivy.lang import Builder

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class AddUserPopup(Popup, EventDispatcher):

    def __init__(self, **kwargs):
        super(AddUserPopup, self).__init__(**kwargs)
        self.register_event_type('on_save')

    def on_save(self, **args):
        pass
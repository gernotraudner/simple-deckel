from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.properties import DictProperty
from kivy.event import EventDispatcher
from kivy.lang import Builder
from store.store import the_store
from widgets.coloredlabel import ColoredLabel
from functools import partial

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class AddItemPanel(BoxLayout, EventDispatcher):
    basket = DictProperty({})

    def __init__(self, **kwargs):
        super(AddItemPanel, self).__init__(**kwargs)
        self.register_event_type('on_save')
        self.bind(basket = self.update_basket)

    def update_basket(self, new_dict, third_arg):
        basket_box = self.ids.basket
        basket_box.clear_widgets()
        panel_basket = self.basket
        for key, value in self.basket.items():
            if value == 0:
                continue
            product = the_store.get_by_id('products', key)
            lbl = ColoredLabel(
                text = ('%s %s [color=#ffffff][b][ref=remove] [ - ] [/ref][ref=add] [ + ] [/ref][/b][/color]' % (value, product['name'])),
                markup = True,
                bg_color = product['color'],
                color = [0, 0, 0, 1])
            lbl.bind(on_ref_press = partial(self.basket_ref_press, product['id'], lbl))
            
            basket_box.add_widget(lbl)
    
    def basket_ref_press(self, product_id, basket_widget, trigger, ref):
        self.basket[product_id] += 1 if ref == 'add' else -1
        if self.basket[product_id] == 0:
            del self.basket[product_id]

    def product_added(self, product_id):
        try:
            dic_entry = self.basket.get(product_id) + 1
        except:
            dic_entry = 1
        self.basket[product_id] = dic_entry

    def abort(self):
        self.basket.clear()

    def save(self):
        self.dispatch('on_save', self.basket)
        self.basket.clear()
        pass

    def on_save(self, basket):
        pass
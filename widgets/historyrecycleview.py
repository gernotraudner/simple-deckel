from kivy.uix.recycleview import RecycleView
from kivy.lang import Builder
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class HistoryRecycleView(RecycleView):
    def __init__(self, **kwargs):
        super(HistoryRecycleView, self).__init__(**kwargs)
        the_store.bind(on_update_selected_customer = self.load_history)
        the_store.bind(on_update_customers = self.load_history)

    def item_to_data_entry(self, item):
        creator = 'Unbekannt'
        try:
            creator = the_store.get_by_id('users', item['createdBy'])['name']
        except:
            pass
        return {
            'h_name': '%s %s' % (item['amount'], item['productName']),
            'h_total': '%.2f €' % item['totalPrice'],
            'h_timestamp': item['timestamp'],
            'h_created_by': creator,
            'h_color': item['color']
        }
    
    def payment_to_data_entry(self, payment):
        creator = 'Unbekannt'
        try:
            creator = the_store.get_by_id('users', payment['createdBy'])['name']
        except:
            pass
        return {
            'h_name': 'Zahlung',
            'h_total': '%.2f €' % payment['amount'],
            'h_timestamp': payment['timestamp'],
            'h_created_by': creator,
            'h_color': [.2, 1, .1, 1]
        }

    def load_history(self, caller):
        customer = the_store.get_by_id('customers', the_store.status['selected_customer'])
        if customer is None:
            return
        items_data = list(map(self.item_to_data_entry, customer['items']))
        payments_data = list(map(self.payment_to_data_entry, customer['payments']))
        sorted_list = sorted(items_data + payments_data, key = lambda history_entry : history_entry['h_timestamp'], reverse = True)
        sorted_list.insert(0, {
            'h_name': 'Beschreibung',
            'h_total': 'Betrag',
            'h_timestamp': 'Datum',
            'h_created_by': 'Eingetragen von',
            'h_color': [0, 0, 0, 0]
        })
        self.data = sorted_list

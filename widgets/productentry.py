from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty, ColorProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ProductEntry(RecycleDataViewBehavior, BoxLayout):
    index = None
    product_id = NumericProperty(-1)
    p_name = StringProperty('')
    p_price = StringProperty('')
    p_category = StringProperty('')
    p_created_by = StringProperty('')
    p_timestamp = StringProperty('')
    p_color = ColorProperty([1, 1, 1, 1])
    change_btn = ObjectProperty(None)
    popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ProductEntry, self).__init__(**kwargs)
        self.popup = Factory.ProductChangePopup()
        self.popup.bind(on_save = self.on_save)
    
    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(ProductEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(ProductEntry, self).refresh_view_layout(rv, index, layout, viewport)

    def on_save(self, instance, **args):
        store = the_store
        product = store.get_by_id('products', self.product_id)
        color = store.get_by_name('categories', args['category'])['color']
        product['name'] = args['name']
        product['price'] = float(args['price'])
        product['category'] = args['category']
        product['color'] = color
        product['createdBy'] = store.status['logged_in_user']['id']
        datetimestr = str(datetime.now())
        product['timestamp'] = datetimestr[:datetimestr.index('.')]
        self.popup.dismiss()
        store.dispatch_update('on_update_products')

    def open_popup(self):
        self.popup.open()
        self.popup.ids.ti_name.text = self.p_name
        self.popup.ids.ti_price.text = self.p_price
        self.popup.ids.ti_category.text = self.p_category

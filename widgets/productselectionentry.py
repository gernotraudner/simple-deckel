from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty, ColorProperty, WeakMethod, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ProductSelectionEntry(RecycleDataViewBehavior, BoxLayout):
    text = StringProperty('')
    color = ColorProperty([0, 0, 0, 0])
    product_id = NumericProperty(-1)
    child = ObjectProperty(None)
    on_release = WeakMethod(lambda *x: x)
    disabled = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(ProductSelectionEntry, self).__init__(**kwargs)
    
    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        if self.child != None:
            self.remove_widget(self.child)
        if data['product_id'] >= 0 and not data['disabled']:
            self.child = Button(
                text = data['text'],
                on_release = data['on_release'])
        else:
            self.child = Label()
        self.add_widget(self.child)
        return super(ProductSelectionEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(ProductSelectionEntry, self).refresh_view_layout(rv, index, layout, viewport)

from kivy.uix.recycleview import RecycleView
from kivy.properties import NumericProperty
from kivy.lang import Builder
from kivy.event import EventDispatcher
from store.store import the_store
from functools import partial

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class ProductSelectionRecycleView(RecycleView, EventDispatcher):
    cur_customer_id = NumericProperty(-1)
    def __init__(self, **kwargs):
        super(ProductSelectionRecycleView, self).__init__(**kwargs)
        self.register_event_type('on_product_added')
        the_store.bind(on_update_products = self.load_products)
        the_store.bind(on_update_selected_customer = self.set_cur_customer_id)
        self.load_products(self)

    def set_cur_customer_id(self, *rest):
        self.cur_customer_id = the_store.status['selected_customer']
        self.load_products(None)

    def product_to_data_entry(self, product = None):
        if product is None:
            return {
                'text': '',
                'product_id': -1,
                'color': [1, 1, 1, 0],
                'on_release': lambda *x: x,
                'disabled': True
            }
        return {
            'text': '%s %.2f €' % (product['name'], product['price']),
            'on_release': partial(self.on_product_button_release, product['id']),
            'product_id': product['id'],
            'color': [0, 0, 0, 0] if self.cur_customer_id < 0 else product['color'],
            'disabled': True if self.cur_customer_id < 0 else False
        }

    def on_product_added(self, caller, product_id):
        pass

    def on_product_button_release(self, product_id, caller):
        self.dispatch('on_product_added', self, product_id)
    
    def load_products(self, caller):
        products_by_cat = {
            'Getränke': [],
            'Mahlzeiten': [],
            'Sonstiges': []
        }

        for product in the_store.products:
            products_by_cat[product['category']].append(product)

        drinks = sorted(products_by_cat['Getränke'], key = lambda product : product['name'])
        drinks_a = drinks[0::2]
        drinks_b = drinks[1::2]
        food = sorted(products_by_cat['Mahlzeiten'], key = lambda product : product['name'])
        other = sorted(products_by_cat['Sonstiges'], key = lambda product : product['name'])
        num_rows = max(len(food), len(drinks_a), len(drinks_b), len(other))
        result_list = []
        for index in range(num_rows):
            result_list.append(drinks_a[index] if len(drinks_a) > index else None)
            result_list.append(drinks_b[index] if len(drinks_b) > index else None)
            result_list.append(food[index] if len(food) > index else None)
            result_list.append(other[index] if len(other) > index else None)
        self.data = list(map(self.product_to_data_entry, result_list))

from kivy.uix.recycleview import RecycleView
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.lang import Builder
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime
from functools import reduce

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class ItemsRecycleView(RecycleView):
    #rv_layout = ObjectProperty(None)
    customers_view = ObjectProperty(None)
    data_sum = NumericProperty(0)
    cur_customer_id = NumericProperty(-1)
    add_items_popup = ObjectProperty(None)
    add_payment_popup = ObjectProperty(None)
    password_popup = ObjectProperty(None)
    history_popup = ObjectProperty(None)
    selected_value = StringProperty('')
    password_callback = None

    def __init__(self, **kwargs):
        super(ItemsRecycleView, self).__init__(**kwargs)
        self.add_payment_popup = Factory.AddPaymentPopup()
        self.history_popup = Factory.HistoryPopup()
        self.add_items_popup = Factory.AddItemsPopup()
        self.password_popup = Factory.PasswordPopup()
        self.add_payment_popup.bind(on_save = self.on_payment_added)
        self.add_items_popup.bind(on_save = self.on_items_added)
        the_store.bind(on_update_customers = self.load_items)
        the_store.bind(on_update_selected_customer = self.load_items)

    def load_items(self, caller):
        # bug here, reproducable like this:
        # select customer franz_ferdinand, manage customers, remove franz ferdinand
        # add new customer, go back
        # first bug: new customer is selected
        # second bug: item list throws exception here
        self.cur_customer_id = the_store.status['selected_customer']
        if self.cur_customer_id == -1:
            items = []
            payments = []
        else:
            customer = the_store.get_by_id('customers', self.cur_customer_id)
            items = list(map(self.item_to_data_entry, customer['items']))
            payments = list(map(self.payment_to_data_entry, customer['payments']))
            self.data = list(filter(lambda data: not data['is_paid'], items + payments))
            self.data_sum = reduce((lambda result, item: result + float(item['price_text'])), items + payments, 0)
            

    def on_customer_changed(self, node, customer_id):
        store = the_store
        self.cur_customer_id = customer_id
        self.load_items(self)
        self.ids.selectable.remove_selection()

    def item_to_data_entry(self, item):
        return {
            'selectable': True,
            'title_text': "%s %s" % (item['amount'], item['productName']),
            'price_text': '-%.2f' % item['totalPrice'],
            'color': item['color'],
            'item_id': str(item['id']),
            'is_paid': item['isPaid']
        }

    def payment_to_data_entry(self, payment):
        return {
            'selectable': True,
            'title_text': 'Zahlung',
            'price_text': '%.2f' % payment['amount'],
            'color': [.2, 1, .2, 1],
            'item_id': str(payment['id']),
            'is_paid': True
        }

    def on_items_added(self, popup, panel, product_ids):
        for key, val in product_ids.items():
            product = the_store.get_by_id('products', key)
            item_name = product['name']
            creator = -1
            try:
                creator = the_store.status['logged_in_user']['id']
            except:
                pass
            datetimestr = str(datetime.now())
            items = the_store.get_by_id('customers', self.cur_customer_id)['items']
            next_item_index = the_store.status['indizes']['nextItemsIndex']
            the_store.status['indizes']['nextItemsIndex'] = next_item_index + 1
            items.append({
                'id': next_item_index,
                'productName': item_name,
                'amount': val,
                'timestamp': datetimestr[:datetimestr.index('.')],
                'createdBy': creator,
                'color': product['color'],
                'totalPrice': product['price'] * val,
                'isPaid': False
            })
        the_store.dispatch_update('on_update_customers')

    def open_add_items_popup(self):
        if self.cur_customer_id == -1:
            return
        self.add_items_popup.open()

    def open_add_payment_popup(self):
        if self.cur_customer_id == -1:
            return
        self.add_payment_popup.ids.ti_amount.text = '%.2f' % max(self.data_sum * -1, +0)
        self.add_payment_popup.open()
    
    def unbind_current_password_popup(self):
        if self.password_callback:
            self.password_popup.unbind(on_approved=self.password_callback)
            self.password_callback = None

    def open_history_popup(self):
        if self.cur_customer_id == -1:
            return
        self.unbind_current_password_popup()
        self.password_callback = lambda *_: self.history_popup.open()
        self.password_popup.bind(on_approved=self.password_callback)
        self.password_popup.open()

    def on_payment_added(self, caller, amount, username):
        customer = the_store.get_by_id('customers', self.cur_customer_id)
        datetimestr = str(datetime.now())
        for item in customer['items']:
            item['isPaid'] = True
            customer['lastTransaction'] = datetimestr[:datetimestr.index('.')]
            self.add_payment_popup.dismiss()

        next_payment_id = the_store.status['indizes']['nextPaymentsIndex']
        creator = 'Unbekannt'
        try:
            creator = the_store.get_by_name('users', username)
        except:
            pass
        the_store.status['indizes']['nextPaymentsIndex'] += 1
        customer['payments'].append({
            'id': next_payment_id,
            'amount': amount,
            'createdBy': creator['id'],
            'timestamp': datetimestr[:datetimestr.index('.')]
        })
        the_store.dispatch_update('on_update_customers')

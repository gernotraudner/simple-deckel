from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.popup import Popup
from kivy.factory import Factory
from kivy.clock import Clock
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ReallyPopup(Popup):
    question_text = StringProperty('')
    def __init__(self, **kwargs):
        self.register_event_type('on_accepted')
        super(ReallyPopup, self).__init__(**kwargs)

    def accept(self):
        self.dismiss()
        self.dispatch('on_accepted')

    def on_accepted(self):
        pass

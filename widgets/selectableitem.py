from kivy.properties import BooleanProperty, ObjectProperty, StringProperty, NumericProperty, ColorProperty
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.factory import Factory
from store.store import the_store
from functools import partial

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class SelectableItem(RecycleDataViewBehavior, BoxLayout):
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    item_id = NumericProperty(-1)
    title_text = StringProperty('')
    price_text = StringProperty('')
    color = ColorProperty([1, 1, 1, 1])
    item_details_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(SelectableItem, self).__init__(**kwargs)
        self.bind(title_text = partial(self.update_size, self.ids.lbl_title))
        self.bind(price_text = partial(self.update_size, self.ids.lbl_price))
        self.item_details_popup = Factory.ItemDetailsPopup()
        self.item_details_popup.bind(on_dismiss = self.on_item_popup_dismiss)

    def on_item_popup_dismiss(self, node):
        self.parent.clear_selection()

    def update_size(self, label, caller, value):
        try:
            float(value)
            value += ' €'
        except:
            pass
        label.text = value

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableItem, self).refresh_view_attrs(rv, index, data)

    def update_popup_data(self, item):
        popup = self.item_details_popup
        popup.item_name = item['productName']
        popup.item_amount = item['amount']
        try:
            category = the_store.get_by_name('products', item['productName'])['category']
        except:
            category = 'Unbekannt'
        popup.item_category = category
        popup.item_total_price = item['totalPrice']
        try:
            user_name = the_store.get_by_id('users', item['createdBy'])['name']
        except:
            user_name = 'Unbekannt'
        popup.item_created_by = user_name
        popup.item_timestamp = item['timestamp']
        popup.item_id = item['id']

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(SelectableItem, self).refresh_view_layout(rv, index, layout, viewport)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableItem, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            customer = the_store.get_by_id('customers', the_store.status['selected_customer'])
            matching_items = list(filter(lambda item : item['id'] == int(self.item_id), customer['items']))
            if (len(matching_items) == 1):
                self.update_popup_data(matching_items[0])
            self.item_details_popup.open()
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if self.parent:
            self.parent.selected_value = str(index)

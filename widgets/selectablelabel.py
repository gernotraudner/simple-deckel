from kivy.properties import BooleanProperty, ObjectProperty, StringProperty, ListProperty, NumericProperty
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from store.store import the_store

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class SelectableLabel(RecycleDataViewBehavior, Label):
    index = None
    c_id = NumericProperty(-1)
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(SelectableLabel, self).__init__(**kwargs)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        if data['c_id'] == the_store.status['selected_customer']:
            # selectable_nodes = rv.layout_manager.get_selectable_nodes()
            # deselect works, but selecting doesn't, probably due to selectable_nodes being empty
            rv.layout_manager.select_node(self)
        return super(SelectableLabel, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(SelectableLabel, self).refresh_view_layout(rv, index, layout, viewport)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if self.parent:
            self.parent.selected_value = self.c_id

class SelectableRecycleBoxLayout(LayoutSelectionBehavior, RecycleBoxLayout):
    """ Adds selection and focus behaviour to the view. """
    selected_value = NumericProperty(-1)
    def remove_selection(self):
        self.selected_value = -1
        self.clear_selection()

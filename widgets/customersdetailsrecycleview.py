from kivy.uix.recycleview import RecycleView
from kivy.lang import Builder
from store.store import the_store
from functools import reduce
Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class CustomersDetailsRecycleView(RecycleView):
    def __init__(self, **kwargs):
        super(CustomersDetailsRecycleView, self).__init__(**kwargs)
        the_store.bind(on_update_customers = self.load_customers)
        self.load_customers(self, True)

    def customer_to_data_entry(self, customer):
        try:
            creator = the_store.get_by_id('users', customer['createdBy'])['name']
        except:
            creator = 'Unbekannt'

        amount = 0
        last_order_index = -1
        last_order_max_id = -1

        for index, item in enumerate(customer['items']):
            amount += item['totalPrice']
            if item['id'] > last_order_max_id:
                last_order_index = index
                last_order_max_id = item['id']
        try:
            last_order_date = customer['items'][last_order_index]['timestamp']
        except:
            last_order_date = 'Nie'

        if len(customer['payments']) > 0:
            last_payment = reduce(lambda latest, payment: latest if payment['id'] < latest['id'] else payment, customer['payments'])
        else:
            last_payment = {'timestamp': 'Nie'}

        return {
            'c_name': customer['name'],
            'c_total': '%.2f €' % reduce(lambda amount, item: amount + item['totalPrice'], customer['items'], 0),
            'c_last_order_date': last_order_date,
            'c_last_payment': last_payment['timestamp'],
            'c_created_by': creator,
            'c_timestamp': customer['timestamp'],
            'c_id': customer['id']
        }
    
    def load_customers(self, caller, initial = False):
        result = list(map(self.customer_to_data_entry, the_store.customers))
        sorted_list = sorted(result, key = lambda customer : customer['c_name'])
        sorted_list.insert(0, {
            'c_name': 'Name',
            'c_total': 'Schuldig',
            'c_last_order_date': 'Zuletzt konsumiert',
            'c_last_payment': 'Zuletzt bezahlt',
            'c_created_by': 'Erstellt von',
            'c_timestamp': 'Erstellt am',
            'c_id': -1
        })
        self.data = sorted_list

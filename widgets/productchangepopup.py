from kivy.uix.popup import Popup
from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.clock import Clock
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ProductChangePopup(Popup, EventDispatcher):
    def __init__(self, **kwargs):
        super(ProductChangePopup, self).__init__(**kwargs)
        self.register_event_type('on_save')
        Clock.schedule_once(lambda dt: self.update_dropdown_vals())
    
    def update_dropdown_vals(self, caller = None):
        self.ids.ti_category.values = list(map(lambda category : category['name'], the_store.categories))

    def on_save(self, **args):
        pass
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.factory import Factory
from store.store import the_store
from datetime import datetime

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class CategoryEntry(RecycleDataViewBehavior, BoxLayout):
    category_id = NumericProperty(-1)
    category_name = StringProperty('')
    delete_btn = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(CategoryEntry, self).__init__(**kwargs)
    
    def refresh_view_attrs(self, rv, index, data):
        return super(CategoryEntry, self).refresh_view_attrs(rv, index, data)

    def refresh_view_layout(self, rv, index, layout, viewport):
        return super(CategoryEntry, self).refresh_view_layout(rv, index, layout, viewport)


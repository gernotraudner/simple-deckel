from kivy.uix.recycleview import RecycleView
from kivy.lang import Builder
from store.store import the_store

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class CategoriesRecycleView(RecycleView):
    def __init__(self, **kwargs):
        super(CategoriesRecycleView, self).__init__(**kwargs)
        self.load_categories(self)
    
    def load_categories(self, caller):
        result = list(map(lambda cat : {'category_name': cat['name'], 'category_id': cat['id']}, the_store.categories))
        sorted_list = sorted(result, key = lambda cat: cat['category_name'])
        self.data = sorted_list
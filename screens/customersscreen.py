from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.factory import Factory
from datetime import datetime
from widgets.productchangepopup import ProductChangePopup
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class CustomersScreen(Screen):
    add_customer_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(CustomersScreen, self).__init__(**kwargs)
        self.add_customer_popup = Factory.AddCustomerPopup()
        self.add_customer_popup.bind(on_save = self.on_customer_added)

    def on_customer_added(self, caller, **args):
        next_customer_index = the_store.status['indizes']['nextCustomersIndex']
        the_store.status['indizes']['nextCustomersIndex'] = next_customer_index + 1
        datetimestr = str(datetime.now())
        the_store.customers.append({
            'id': next_customer_index,
            'name': args['name'],
            'timestamp': datetimestr[:datetimestr.index('.')],
            'lastTransaction': datetimestr[:datetimestr.index('.')],
            'createdBy': the_store.status['logged_in_user']['id'],
            'items': [],
            'payments': []
        })
        self.add_customer_popup.dismiss()
        the_store.dispatch_update('on_update_customers')

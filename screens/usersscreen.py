from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.factory import Factory
from datetime import datetime
from widgets.productchangepopup import ProductChangePopup
from store.store import the_store
from tools.hashing import hash_password

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class UsersScreen(Screen):
    add_user_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(UsersScreen, self).__init__(**kwargs)
        self.add_user_popup = Factory.AddUserPopup()
        self.add_user_popup.bind(on_save = self.on_user_added)

    def on_user_added(self, caller, **args):
        next_user_index = the_store.status['indizes']['nextUsersIndex']
        the_store.status['indizes']['nextUsersIndex'] = next_user_index + 1
        datetimestr = str(datetime.now())
        the_store.users.append({
            'id': next_user_index,
            'name': args['name'],
            'password': hash_password(args['password']),
            'timestamp': datetimestr[:datetimestr.index('.')],
            'createdBy': the_store.status['logged_in_user']['id']
        })
        self.add_user_popup.dismiss()
        the_store.dispatch_update('on_update_users')

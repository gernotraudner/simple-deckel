from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.factory import Factory
from datetime import datetime
from widgets.productchangepopup import ProductChangePopup
from store.store import the_store

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class ProductsScreen(Screen):
    add_product_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ProductsScreen, self).__init__(**kwargs)
        self.add_product_popup = Factory.ProductChangePopup()
        self.add_product_popup.bind(on_save = self.on_product_added)

    def on_product_added(self, caller, **args):
        next_product_index = the_store.status['indizes']['nextProductsIndex']
        the_store.status['indizes']['nextProductsIndex'] = next_product_index + 1
        color = the_store.get_by_name('categories', args['category'])['color']
        datetimestr = str(datetime.now())
        the_store.products.append({
            'id': next_product_index,
            'name': args['name'],
            'price': float(args['price']),
            'category': args['category'],
            'color': color,
            'timestamp': datetimestr[:datetimestr.index('.')],
            'createdBy': the_store.status['logged_in_user']['id']
        })
        self.add_product_popup.dismiss()
        the_store.dispatch_update('on_update_products')

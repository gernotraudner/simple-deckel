from kivy.uix.screenmanager import Screen
from kivy.lang import Builder

Builder.load_file('templates/%s.kv' % __name__.replace('.', '/'))

class LoginScreen(Screen):
    def __init__(self, **kwargs):
        super(LoginScreen, self).__init__(**kwargs)

    def login(self, *args):
        self.manager.transition.direction = 'left'
        self.manager.current = 'overview'

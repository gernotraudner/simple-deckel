from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.clock import Clock
from store.store import the_store
from kivy.factory import Factory
from functools import partial

with open('templates/%s.kv' % __name__.replace('.', '/'), encoding='utf8') as f:
    Builder.load_string(f.read())

class OverviewScreen(Screen):
    last_persistance = StringProperty('')
    password_popup = ObjectProperty(None)
    cur_pw_callback = None

    def __init__(self, **kwargs):
        super(OverviewScreen, self).__init__(**kwargs)
        self.password_popup = Factory.PasswordPopup()
        the_store.bind(on_update_persist = lambda x: self.update_login_label(0))

    def open_password_popup(self, target):
        if self.cur_pw_callback:
            self.password_popup.unbind(on_approved=self.cur_pw_callback)
        self.cur_pw_callback = partial(self.set_manager_target, target) 
        self.password_popup.bind(on_approved=self.cur_pw_callback)
        self.password_popup.open()

    def set_manager_target(self, target, password_popup, username):
        user = the_store.get_by_name('users', username)
        the_store.status['logged_in_user']['id'] = user['id']
        the_store.status['logged_in_user']['name'] = user['name']
        the_store.dispatch_update('on_update_logged_in_user')
        self.manager.transition.direction = 'left'
        self.manager.current = target

    def update_login_label(self, dt):
        self.last_persistance = the_store.status['last_persistance']

    def logout(self):
        the_store.logout()
        self.manager.transition.direction = 'right'
        self.manager.current = 'login'
